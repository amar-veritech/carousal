document.addEventListener('DOMContentLoaded', () => {
  const carouselInner = document.getElementById('carousel-inner')
  const carouselDots = document.getElementById('carousel-dots')
  const intervalTime = 3000
  let currentIndex = 0

  const imageUrls = [
    'assets/image1.jpeg',
    'assets/image2.jpeg',
    'assets/image3.jpeg',
    'assets/image4.jpeg',
    'assets/image5.jpeg',
    'assets/image6.jpeg',
  ]

  async function fetchData() {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/todos')
      const data = await response.json()
      return data
    } catch (error) {
      console.error('Error fetching data:', error)
    }
  }

  function createCarouselItems(todos) {
    todos.slice(0, 6).forEach((todo, index) => {
      const itemDiv = document.createElement('div')
      itemDiv.classList.add('carousel-item')
      itemDiv.style.backgroundImage = `url(${imageUrls[index]})`

      const captionDiv = document.createElement('div')
      captionDiv.classList.add('caption')
      captionDiv.innerText = todo.title

      itemDiv.appendChild(captionDiv)
      carouselInner.appendChild(itemDiv)

      const dot = document.createElement('span')
      dot.classList.add('dot')
      if (index === 0) {
        dot.classList.add('active')
      }
      carouselDots.appendChild(dot)
    })
  }

  function updateDots() {
    const dots = document.querySelectorAll('.dot')
    dots.forEach((dot, index) => {
      dot.classList.toggle('active', index === currentIndex)
    })
  }

  function startCarousel() {
    const items = document.querySelectorAll('.carousel-inner .carousel-item')
    if (items.length > 0) {
      setInterval(() => {
        currentIndex = (currentIndex + 1) % items.length
        const offset = -currentIndex * 100
        carouselInner.style.transform = `translateX(${offset}%)`
        updateDots()
      }, intervalTime)
    }
  }

  async function initCarousel() {
    const data = await fetchData()
    if (data) {
      createCarouselItems(data)
      startCarousel()
    }
  }

  initCarousel()
})
